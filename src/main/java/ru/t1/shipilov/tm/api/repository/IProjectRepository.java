package ru.t1.shipilov.tm.api.repository;

import ru.t1.shipilov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    Project create(String name, String description);

    Project create(String name);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    int getSize();

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
